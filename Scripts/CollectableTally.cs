﻿using UnityEngine;
using System.Collections;

public class CollectableTally : MonoBehaviour 
{
	public int tally = 0;
	
	public int startTime = 0;
	
	void Start()
	{
		startTime = (int) Time.time;
	}
	
	public void restart()
	{
		startTime = (int) Time.time;
	}
	
	public int getFinalTime( int time )
	{
		return time - startTime;
	}
	
	void OnGUI()
	{		
		GUI.Label( new Rect( 15, 15, 50, 100 ), "Time: " + ((int)Time.time - startTime) );
	}
}
