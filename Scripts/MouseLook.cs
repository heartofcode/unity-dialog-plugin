using UnityEngine;
using System.Collections;

//Currently does not have the code for snapping

public class MouseLook : MonoBehaviour {

	public Transform cameraTransform;
	private Transform target;

	//Distance in x-z plane to target
	public float distance = 7.0f;

	//height we want to camera to be above target
	public float height = 3.0f;

	public float angularSmoothLag=0.3f;
	public float angularMaxSpeed=15.0f;

	public float heightSmoothLag = 0.3f;

	public float snapSmoothLag= 0.2f;
	public float snapMaxSpeed= 720.0f;
	
	public float clampHeadPositionScreenSpace= 0.75f;
	
	public float lockCameraTimeout= 0.2f;
	
	private Vector3 headOffset= Vector3.zero;
	private Vector3 centerOffset= Vector3.zero;
	
	private float heightVelocity= 0.0f;
	private float angleVelocity= 0.0f;
	private bool snap= false;
	private Component controller;
	private float targetHeight= 100000.0f; 

	void  Awake (){

		target = transform;

		if (!cameraTransform && Camera.main)
			cameraTransform = GameObject.FindWithTag ("MainCamera").transform;
		if(!cameraTransform) {
			Debug.Log("Unable to find Camera.");
			enabled = false;	
		}
		
		

		if (target)
		{
			controller = target.GetComponent<CharacterMovement>();
		} 
		else 
			Debug.Log("Unable to find target.");

		centerOffset = GetComponent<Rigidbody>().GetComponent<Collider>().bounds.center - target.position;
		headOffset = centerOffset;
		headOffset.y = GetComponent<Rigidbody>().GetComponent<Collider>().bounds.max.y - target.position.y;

		//Insert Cut Function here
	}

	public float AngleDistance ( float a ,   float b  ){
		a = Mathf.Repeat(a, 360);
		b = Mathf.Repeat(b, 360);
		
		return Mathf.Abs(b - a);
	}

	void  Apply ( Transform dummyTarget ,   Vector3 dummyCenter  ){
		// Early out if we don't have a target
		if (!controller)
			return;
		
		Vector3 targetCenter= target.position + centerOffset;
		Vector3 targetHead= target.position + headOffset;
		
		//	DebugDrawStuff();
		
		// Calculate the current & target rotation angles
		float originalTargetAngle= target.eulerAngles.y;
		float currentAngle= cameraTransform.eulerAngles.y;
		
		// Adjust real target angle when camera is locked
		float targetAngle= originalTargetAngle; 
		

//		if (controller.GetLockCameraTimer () < lockCameraTimeout)
//		{
//			targetAngle = currentAngle;
//		}
		
		// Lock the camera when moving backwards!
		// * It is really confusing to do 180 degree spins when turning around.
//		if (AngleDistance (currentAngle, targetAngle) > 160 && controller.isMovingBackwards())
//			targetAngle += 180;
		
		currentAngle = Mathf.SmoothDampAngle(currentAngle, targetAngle, ref angleVelocity, angularSmoothLag, angularMaxSpeed);
		
		
		// When jumping don't move camera upwards but only down!
//		if (controller.IsJumping ())
//		{
//			// We'd be moving the camera upwards, do that only if it's really high
//			float newTargetHeight= targetCenter.y + height;
//			if (newTargetHeight < targetHeight || newTargetHeight - targetHeight > 5)
//				targetHeight = targetCenter.y + height;
//		}
//		// When walking always update the target height
//		else
//		{
		targetHeight = targetCenter.y + height;
//		}
		
		// Damp the height
		float currentHeight= cameraTransform.position.y;
		currentHeight = Mathf.SmoothDamp (currentHeight, targetHeight, ref heightVelocity, heightSmoothLag);
		
		// Convert the angle into a rotation, by which we then reposition the camera
		Quaternion currentRotation= Quaternion.Euler (0, currentAngle, 0);
		
		// Set the position of the camera on the x-z plane to:
		// distance meters behind the target
		cameraTransform.position = targetCenter;
		cameraTransform.position += currentRotation * Vector3.back * distance;
		
		Vector3 tempCameraTransformPos=cameraTransform.position;
		
		tempCameraTransformPos.y=currentHeight;
		cameraTransform.position=tempCameraTransformPos;
		
		// Always look at the target	
//		SetUpRotation(targetCenter, targetHead);
	}

	void  LateUpdate (){
		Apply (transform, Vector3.zero);
	}
}