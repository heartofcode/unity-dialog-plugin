using UnityEngine;
using System.Collections;

public class Collectable : MonoBehaviour
{
	private enum statuses { open, shut };
	private statuses status = statuses.shut;
	
	public int openAngle = 360;
	public int shutAngle = 0;
	public int swingSpeed = 20;
	
	private int angle = 0;
	private bool rotating = false;
	private Vector3 curEuler;
	
	private AudioSource[] audio;
	
	void Start()
	{
		audio = Camera.main.GetComponents<AudioSource>();
		
		var num = (int) Random.Range (0,16);
		
		if ( num < 4 )
		{
			openAngle *= -1;
		}
		else if ( num < 8 )
		{
			shutAngle = 360;
			openAngle = 0;
		}
		else if ( num < 12 )
		{
			shutAngle = -360;
			openAngle = 0;
		}
	}
	
	void Update()
	{
		if ( rotating )
		{
			Rotate( angle );
		}
		else
		{
			switch( status )
			{
			case statuses.open:
				Close();
				break;
				
			case statuses.shut:
				Open();
				break;
			}
		}
	}
	
	void OnTriggerEnter( Collider other )
	{
		if ( other.CompareTag("Player") )
		{
			audio[2].Play();
			other.GetComponent<CollectableTally>().tally += 1;
			Destroy ( gameObject );
		}
	}
	
	void Open()
	{		
		status = statuses.open;
		angle = openAngle;
		
		rotating = true;
	}
	
	void Close()
	{
		status = statuses.shut;
		angle = shutAngle;
		
		rotating = true;
	}
	
	void Rotate( float angle )
	{	
		var newAngle = gameObject.transform.rotation.y + angle;
		
		if ( curEuler.y != newAngle )
		{
			curEuler.y = Mathf.MoveTowards( curEuler.y, newAngle, swingSpeed*Time.deltaTime );
			transform.eulerAngles = curEuler;
			
			return;
		}
		
		rotating = false;
	}
}
