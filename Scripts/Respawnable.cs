﻿using UnityEngine;
using System.Collections;

public class Respawnable : MonoBehaviour {
	
	public Vector3 currentRespawnLoc = new Vector3(0, 0, 0);
	
	// Use this for initialization
	void Start () 
	{
		currentRespawnLoc = gameObject.transform.position;
	}
	
	public void RespawnObject ()
	{
		gameObject.transform.position = currentRespawnLoc;
	}
}
