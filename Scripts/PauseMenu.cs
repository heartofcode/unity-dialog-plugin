﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour {

	bool paused = false;
	
	void Update () 
	{
		if ( Input.GetKeyDown( KeyCode.Escape ) )
		{
			Pause();
		}
	}
	
	void Pause()
	{
		paused = !paused;
		Time.timeScale = 1.0f - Time.timeScale;
	}
	
	void OnGUI()
	{
		if ( paused )
		{
			var rect = new Rect( 10, 10, 200, 35 );
			rect.center = new Vector2( Screen.width/2, Screen.height/2 - 70 );
			
			if ( GUI.Button( rect, "Return to Game" ) )
			{
				Pause();
			}
			
			rect.center = new Vector2( rect.center.x, rect.center.y + 70 );
			
			if ( GUI.Button ( rect, "Respawn at last checkpoint" ) )
			{
				Pause();
				gameObject.GetComponent<Health>().respawn();
			}
			
			rect.center = new Vector2( rect.center.x, rect.center.y + 70 );
			
			if ( GUI.Button( rect, "Quit to Main Menu" ) )
			{
				Pause();
				Application.LoadLevel( "MainMenu" );
			}
		}
	}
}
