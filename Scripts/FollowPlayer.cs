﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {

	Transform target;
	Transform myTransform;
	Animation myAnimator;
	
	public int moveSpeed = 0;
	public int rotationSpeed = 0;
	public int minDistance = 5;
	public int aggroRange = 10;
	public int meleeRange = 5;
	
	bool attacking;

	// Use this for initialization
	void Start () 
	{
		myTransform = this.transform;
		myAnimator = this.GetComponent<Animation>();
		target = GameObject.FindWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update () 
	{
		var distance = Vector3.Distance(target.position, myTransform.position);
		
		if ( distance > minDistance && distance < aggroRange )
		{
			moveToPlayer();
		}
		
		if ( distance < meleeRange )
		{
			meleeAttack();
		}
	}
	
	void meleeAttack()
	{
		myAnimator.Play ( "Attack" );
	}
	
	void moveToPlayer()
	{
		//rotate to look at the player
		myTransform.rotation = Quaternion.Slerp( myTransform.rotation,
		                                        Quaternion.LookRotation(target.position - myTransform.position), 
		                                        rotationSpeed*Time.deltaTime );
		//move towards the player
		myTransform.position += myTransform.forward * moveSpeed * Time.deltaTime;
		
		myTransform.localEulerAngles = Vector3.Scale ( new Vector3( 0, 1, 0 ), myTransform.localEulerAngles );
		
		myAnimator.Play ( "Run" );
	}
	
} // end class
