﻿using UnityEngine;
using System.Collections;

public class TimedDestroy : MonoBehaviour {

	public float lifeSpan = 5; //5 seconds
	float currLifeSpan;

	// Use this for initialization
	void Start () {
		currLifeSpan = lifeSpan;
	}
	
	// Update is called once per frame
	void Update () {
		currLifeSpan -= Time.deltaTime;
		if (currLifeSpan < 0) 
			Destroy (gameObject);
	}
}
