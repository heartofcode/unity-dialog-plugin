﻿using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor (typeof(Dialog))]
public class DialogInspector : Editor {

	Dialog myTarget;
	
	void OnEnable()
	{
		myTarget = (Dialog) target;
		myTarget.LoadDialog();
	}
	
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		
		if ( GUILayout.Button("Open Dialog Window") && !DialogWindow.IsOpen )
		{
			if ( myTarget.xmlFilename == "" )
			{
				myTarget.xmlFilename = myTarget.gameObject.name;
			}
			AssetDatabase.Refresh ();
			DialogWindow.ShowEditor(myTarget);
		}
		
		// TO-DO:
		// Add in an array that stores available gameobject speakers, use this to generate list of speakers in the XML
		// store array index in XML, load that index into the node in node editor
		
		// for items in speakers List where index < 2: gui.enabled = false
	}
}
