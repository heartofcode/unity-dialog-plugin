﻿using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor (typeof(DialogController))]
public class DialogControllerInspector : Editor {

	DialogController myTarget;
	
	void OnEnable()
	{
		myTarget = (DialogController) target;
	}

	public override void OnInspectorGUI()
	{		
		/*--------------------------------------------------------------------
			Basic Dialog Options:
				GUISkin
				Maximum engage distance
		*///------------------------------------------------------------------
		
		EditorGUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel("Dialog GUI:");
			myTarget.guiSkin = (GUISkin) EditorGUILayout.ObjectField ( myTarget.guiSkin, typeof(GUISkin), false );
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel("Max Engage Distance:");
			myTarget.maxEngageDistance = (float) EditorGUILayout.FloatField( myTarget.maxEngageDistance );
		EditorGUILayout.EndHorizontal();
		
		/*--------------------------------------------------------------------
			Choice-Node Dislay Options
		*///------------------------------------------------------------------
		
		EditorGUILayout.Separator();
		EditorGUILayout.HelpBox( "\nChoice-Node Display Options\n", MessageType.None );
		EditorGUILayout.Separator();
		
		myTarget.screenDivisor = (Vector2) EditorGUILayout.Vector2Field( "Screen Divisor:", myTarget.screenDivisor );
		
		myTarget.shape = (DialogController.GUIShape) EditorGUILayout.EnumPopup( myTarget.shape );
		
		/*--------------------------------------------------------------------
			Circular-Choice Display Options
		*///------------------------------------------------------------------
		
		if ( myTarget.shape == DialogController.GUIShape.Circular )
		{
			EditorGUILayout.BeginHorizontal();
				EditorGUILayout.PrefixLabel("Total Arc:");
				myTarget.arc = EditorGUILayout.IntField( myTarget.arc );
			EditorGUILayout.EndHorizontal();
			
			EditorGUILayout.BeginHorizontal();
				EditorGUILayout.PrefixLabel("Degree Offset:");
				myTarget.degreeOffset = EditorGUILayout.IntField( myTarget.degreeOffset );
			EditorGUILayout.EndHorizontal();
			
			myTarget.spaceOffset = EditorGUILayout.Vector2Field( "Spacing Offset:", myTarget.spaceOffset );
			
			EditorGUILayout.BeginHorizontal();
				EditorGUILayout.PrefixLabel("Counter-Clockwise:");
				myTarget.counterclock = EditorGUILayout.Toggle( myTarget.counterclock );
			EditorGUILayout.EndHorizontal();
		}
		
		/*--------------------------------------------------------------------
			List-Choice Dislay Options
		*///------------------------------------------------------------------
		
		else if ( myTarget.shape == DialogController.GUIShape.List )
		{
			myTarget.choiceboxSize = EditorGUILayout.Vector2Field( "Choicebox GUI Size:", myTarget.choiceboxSize );

			myTarget.buffer = EditorGUILayout.Vector2Field( "Choicebox Buffer:", myTarget.buffer );
			
			EditorGUILayout.BeginHorizontal();
				EditorGUILayout.PrefixLabel("Num. Columns");
				myTarget.columns = EditorGUILayout.IntField( myTarget.columns );
			EditorGUILayout.EndHorizontal();
		}
	}
}
